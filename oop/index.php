<?php 

require('Animal.php');
require('Frog.php');
require('Ape.php');

// Release 0
echo "RELEASE 0 <br>";
$sheep = new Animal("shaun");
echo "Name: " . $sheep->get_name() . "<br>"; // "shaun"
echo "Legs: " . $sheep->get_legs() . "<br>"; // 4
echo "Cold blooded: " . $sheep->get_cold_blooded() . "<br>"; // "no"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())


// Release 1
echo "<br><br>RELEASE 1 <br>";
$sungokong = new Ape("kera sakti");
echo "Name: " . $sungokong->get_name() . "<br>"; 
echo "Legs: " . $sungokong->get_legs() . "<br>"; 
echo "Cold blooded: " . $sungokong->get_cold_blooded() . "<br>"; 
echo "Yell: ";
$sungokong->yell(); // "Auooo"
echo "<br>";
$kodok = new Frog("buduk");
echo "Name: " . $kodok->get_name() . "<br>"; 
echo "Legs: " . $kodok->get_legs() . "<br>"; 
echo "Cold blooded: " . $kodok->get_cold_blooded() . "<br>"; 
echo "Jump: ";
$kodok->jump();
echo "<br>"; 
?>